import React, {Component} from 'react';
import "bootstrap/dist/css/bootstrap.min.css"
import Card from "./components/card/card"
import "./App.css"

class App extends Component {
    render() {
        return (
            <div className="container wrapper">
                <Card />
            </div>
        );
    }
}

export default App;