import React, {useEffect, useState } from 'react';
import Tilt from "react-vanilla-tilt";
import "./card.scss"
import {java_1,java_2,python_1,python_2,flutter_1,flutter_2,ios_1,ios_2,arrowRight,arrowLeft} from "./icons";
import { useSpring, animated } from "react-spring";
import {BrowserRouter as Router, NavLink} from "react-router-dom";
import "../media.scss"

const all = [
    {id:1,name:"python tilt", title:"Full-stack Python developer",body:"Web dasturlash brauzeringiz orqali ko'rishingiz va foydalanishingiz mumkin bo'lgan barcha web dasturlarni tayyorlash jarayonini oz ichiga oladi",class: "card1", icon1: python_1(),icon2: python_2(), to:"/python"},
    {id:2,name: "java tilt", title:"Full-stack Java developer",body:"Web dasturlash brauzeringiz orqali ko'rishingiz va foydalanishingiz mumkin bo'lgan barcha web dasturlarni tayyorlash jarayonini oz ichiga oladi",class: "card2", icon1: java_1(),icon2: java_2(),to:"/java"},
    {id:3,name: "ios tilt",title:"IOS developer",body:"Web dasturlash brauzeringiz orqali ko'rishingiz va foydalanishingiz mumkin bo'lgan barcha web dasturlarni tayyorlash jarayonini oz ichiga oladi",class: "card3", icon1: ios_1(),icon2: ios_2(),to:"/ios"},
    {id:4,name:"flutter tilt",title:"Flutter developer",body:"Web dasturlash brauzeringiz orqali ko'rishingiz va foydalanishingiz mumkin bo'lgan barcha web dasturlarni tayyorlash jarayonini oz ichiga oladi",class: "card4", icon1: flutter_1(), icon2: flutter_2(), to:"/flutter"},
    {id:5,name: "java tilt",title:"Full-stack Java developer",body:"Web dasturlash brauzeringiz orqali ko'rishingiz va foydalanishingiz mumkin bo'lgan barcha web dasturlarni tayyorlash jarayonini oz ichiga oladi",class: "card5", icon1: java_1(),icon2: java_2(), to:"/java"},
    {id:6,name: "flutter tilt",title:"Flutter developer",body:"Web dasturlash brauzeringiz orqali ko'rishingiz va foydalanishingiz mumkin bo'lgan barcha web dasturlarni tayyorlash jarayonini oz ichiga oladi",class: "card6", icon1: flutter_1(),icon2: flutter_2(), to:"/flutter"},
    {id:7,name: "ios tilt",title:"IOS developer",body:"Web dasturlash brauzeringiz orqali ko'rishingiz va foydalanishingiz mumkin bo'lgan barcha web dasturlarni tayyorlash jarayonini oz ichiga oladi",class: "card7", icon1: ios_1(),icon2: ios_2(), to:"/ios"},
    {id:8,name: "flutter tilt",title:"Flutter developer",body:"Web dasturlash brauzeringiz orqali ko'rishingiz va foydalanishingiz mumkin bo'lgan barcha web dasturlarni tayyorlash jarayonini oz ichiga oladi",class: "card8", icon1: flutter_1(),icon2: flutter_2(), to:"/flutter"},
    {id:9,name: "ios tilt",title:"IOS developer",body:"Web dasturlash brauzeringiz orqali ko'rishingiz va foydalanishingiz mumkin bo'lgan barcha web dasturlarni tayyorlash jarayonini oz ichiga oladi",class: "card9", icon1: ios_1(),icon2: ios_2(), to:"/ios"}
]


export default function Card() {
    const [step, setStep] = useState(0)
    const [margin, setMargin] = useState(false);

    const startCarousel = useSpring({
        transform: `translateX(-${step*330}px)`,
        gridColumnGap : margin ? "7rem" : "3.8rem",
        config: {
            duration: 500,
        }
    })

    useEffect(()=>{
        if (step > 0){
            setMargin(true);
            setTimeout(()=>{
                setMargin(false);
            },500);
        }
    },[step]);

    const handleLeft =()=>{
        if (step > 0) setStep(step-1)
    };

    const handleRight =()=>{
        if (window.innerWidth > 668) {
            if (step < all.length - 1) setStep(step + 1)
        }else {
            if (step < all.length - 1) setStep(step + 1)
        }
    }

    window.addEventListener("resize", () => {
        setStep(0);
    });

    return (
            <div className="App">
                <div className="d-flex">
                    <div className="div">
                        <h1>Mutaxasislikni tanlang</h1>
                        <p>Dasturlashni endi o‘rganayotganlar uchun istalgan bir sohada o‘zlashtirilishi kerak bo‘lgan bilimlar ketma-ketligi hamda bu sohaning istiqboli aniq dalillar bilan ko‘rsatib beradi.</p>
                    </div>
                    <div className="d-flex">
                        <div className="left" onClick={()=>handleLeft()}>
                            {arrowLeft()}
                        </div>
                        <div className="right" onClick={()=>handleRight()}>
                            {arrowRight()}
                        </div>
                    </div>
                </div>
                <div className="App-header">
                    <div className="flex-container d-flex">
                        <animated.div style={startCarousel} className="card-content d-flex">
                            {
                                all.map(item=>(
                                    <Tilt className={item.name}>
                                        <div className="second">
                                            {item.icon2}
                                        </div>
                                        <div className="first">
                                            {item.icon1}
                                        </div>
                                        <h4>{item.title}</h4>
                                        <p className="opacity">{item.body}</p>
                                        <Router>
                                            <NavLink to={item.to} className="navLink">Batafsil</NavLink>
                                        </Router>
                                    </Tilt>
                                ))
                            }
                        </animated.div>
                    </div>
                </div>
            </div>
        );
}

